﻿using System;

namespace lecture_EF.BLL.DTO
{
    public class UserInfoDto
    {
        public int UserId { get; set; }
        public int LastProjectId { get; set; }

        public int AllTasks { get; set; }
        public int AllUnfinishedTasks { get; set; }

        public int LongestTaskId { get; set; }
    }
}
