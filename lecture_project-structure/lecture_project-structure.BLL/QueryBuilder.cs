﻿using System;
using lecture_EF.DAL.Entites;
using System.Collections.Generic;
using System.Linq;
using lecture_EF.DAL.Interfaces;
using lecture_project_structure.BLL.Interfaces;
using System.Threading.Tasks;
using System.Threading;

namespace lecture_EF.BLL
{
    public class QueryBuilder : IQueryBuilder
    {
        private IEnumerable<Project> _projectsList;
        private IEnumerable<User> _usersList;
        private IEnumerable<Team> _teamsList;
        private IEnumerable<MyTask> _tasksList;
        private IEnumerable<Project> _hierarchy;
        private IUnitOfWork _unitOfWork;

        public QueryBuilder(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task UpdateInformation()
        {
            _projectsList = await _unitOfWork.Projects.ReadAll();
            _usersList = await _unitOfWork.Users.ReadAll();
            _teamsList = await _unitOfWork.Teams.ReadAll();
            _tasksList = await _unitOfWork.Tasks.ReadAll();
            await GetHierarchy();
        }

        public async Task GetHierarchy()
        {
            _hierarchy = _projectsList.GroupJoin(_tasksList, p => p.Id, t => t.ProjectId,
                (p, t) => new Project()
                {
                    Id = p.Id,
                    AuthorId = p.AuthorId,
                    TeamId = p.TeamId,
                    Name = p.Name,
                    Description = p.Description,
                    Deadline = p.Deadline,
                    CreatedAt = p.CreatedAt,
                    Tasks = t.GroupJoin(_usersList, t => t.PerformerId, u => u.Id,
                        (t, u) => new MyTask()
                        {
                            Id = t.Id,
                            ProjectId = t.ProjectId,
                            PerformerId = t.PerformerId,
                            Performer = _usersList.Where(u => u.Id == t.PerformerId).FirstOrDefault(),
                            Name = t.Name,
                            Description = t.Description,
                            State = t.State,
                            CreatedAt = t.CreatedAt,
                            FinishedAt = t.FinishedAt
                        }
                        ).ToList(),
                    Team = _teamsList.Where(t => t.Id == p.TeamId).FirstOrDefault(),
                    Author = _usersList.Where(u => u.Id == p.AuthorId).First()
                })
                .ToList();
        }

        public async Task<Dictionary<string, int>> GetTasksInProject(int Id)
        {
            await UpdateInformation();
            return _hierarchy
                .Where(u => u.AuthorId == Id)
                .Select(t => new { projectName = t.Name, taskCunt = t.Tasks.Count() })
                .ToDictionary(x => x.projectName, x => x.taskCunt);
        }

        public async Task<List<MyTask>> GetTasksByUser(int Id)
        {
            await UpdateInformation();
            return _hierarchy
               .SelectMany(t => t.Tasks)
               .Where(t => t.PerformerId == Id && t.Name.Length < 45)
               .ToList();
        }
        public async Task<List<MyTaskInfo>> GetFinishedTasksByUser(int Id)
        {
            await UpdateInformation();
            return _hierarchy
               .SelectMany(t => t.Tasks)
               .Where(t => t.PerformerId == Id && t.FinishedAt != null)
               .Select(t => new MyTaskInfo { Id = t.Id, Name = t.Name }).ToList();
        }

        public async Task<IEnumerable<IGrouping<int?, User>>> GetUsersFromTeam()
        {
            await UpdateInformation();
            return _hierarchy
               .SelectMany(t => t.Tasks)
               .Select(u => u.Performer)
               .Where(u => (DateTime.Now.Year - u.BirthDay.Year) > 10)
               .Distinct().OrderBy(y => y.RegisteredAt)
               .GroupBy(t => t.TeamId).ToList();
        }

        public async Task<List<User>> GetUsersByName()
        {
            await UpdateInformation();
            return _hierarchy
               .SelectMany(t => t.Tasks)
               .OrderByDescending(t => t.Name)
               .Select(u => u.Performer)
               .Distinct()
               .OrderBy(y => y.FirstName)
               .ToList();
        }
        public async Task<UserInfo> GetUserInfo(int Id)
        {
            await UpdateInformation();
            return _hierarchy
               .Select(p => new UserInfo
               {
                   User = _hierarchy
                           .SelectMany(t => t.Tasks)
                           .Select(u => u.Performer)
                           .Where(u => u.Id == Id)
                           .Select(u => u)
                           .First(),
                   LastProject = _hierarchy
                            .Where(u => u.AuthorId == Id)
                            .OrderBy(x => x.CreatedAt)
                            .First(),
                   AllTasks = _hierarchy
                            .Where(u => u.AuthorId == Id)
                            .OrderBy(x => x.CreatedAt)
                            .First().Tasks
                            .Count(),
                   AllUnfinishedTasks = _hierarchy
                           .SelectMany(t => t.Tasks)
                           .Where(t => t.PerformerId == Id && t.FinishedAt == null)
                           .Count(),
                   LongestTask = _hierarchy
                           .SelectMany(t => t.Tasks)
                           .Where(t => t.PerformerId == Id)
                           .OrderByDescending(x => (x.FinishedAt ?? DateTime.Now) - x.CreatedAt)
                           .First()
               }).ToList()[0];
        }

        public async Task<List<ProjectInfo>> GetProjectInfo()
        {
            await UpdateInformation();
            return _hierarchy
               .Select(p => new ProjectInfo
               {
                   Project = p,
                   LongestTask = p.Tasks
                       .OrderByDescending(x => x.Description.Length)
                       .FirstOrDefault(),
                   ShortestTask = p.Tasks
                        .OrderBy(x => x.Name.Length)
                        .FirstOrDefault(),
                   AllUsersOnProject = p.Tasks
                        .Where(t => p.Description.Length > 20 || p.Tasks.Count() < 3)
                        .Select(t => t.Performer)
                        .Count()
               }).ToList();
        }

        public async Task<List<MyTask>> GetUnfinishedTasksByUser(int id)
        {
            await UpdateInformation();
            return _tasksList.Where(t => t.PerformerId == id && t.FinishedAt == null).ToList();
        }
    }
}
