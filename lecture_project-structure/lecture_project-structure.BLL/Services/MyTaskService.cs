﻿using lecture_EF.BLL.Interfaces;
using lecture_EF.DAL.Entites;
using lecture_EF.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace lecture_EF.BLL.Services
{
    public class MyTaskService : IMyTaskService
    {
        IUnitOfWork Database { get; set; }

        public MyTaskService(IUnitOfWork unitOfWork)
        {
            Database = unitOfWork;
        }
        public async Task CreateTask(MyTask task)
        {
            if (task == null)
                throw new NullReferenceException("Task is empty");

            List<ValidationResult> results = new List<ValidationResult>();
            ValidationContext context = new ValidationContext(task);
            string errors = null;
            
            if (!Validator.TryValidateObject(task, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                throw new ArgumentException(errors);
            }
            else
            {
                await Database.Tasks.Create(task);
                await Database.SaveAsync();
            }
        }

        public async Task DeleteTask(string id)
        {
            int _id;
            if (int.TryParse(id, out _id) && await Database.Tasks.Read(_id) != null)
            {
                await Database.Tasks.Delete(_id);
            }
            else
                throw new ArgumentException("Invalid Id");            
        }

        public async Task DisposeAsync()
        {
            await Database.DisposeAsync();
        }

        public async Task<IEnumerable<MyTask>> GetAllTasks()
        {
            return await Database.Tasks.ReadAll();
        }

        public async Task<MyTask> GetTask(string id)
        {
            int _id;
            if (int.TryParse(id, out _id))
                return await Database.Tasks.Read(_id);
            else
                throw new ArgumentException("Invalid Id");
        }

        public async Task ChangeTaskState(MyTask task)
        {
            if (task == null)
                throw new NullReferenceException("Task is empty");

            List<ValidationResult> results = new List<ValidationResult>();
            ValidationContext context = new ValidationContext(task);
            string errors = null;

            if (!Validator.TryValidateObject(task, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                throw new ArgumentException(errors);
            }
            else if(task.FinishedAt != null)
            {
                throw new ArgumentException("Task is finished");
            }
            else
            {
                task.FinishedAt = DateTime.Now;
                await Database.Tasks.Update(task);
            }
        }

        public async Task UpdateTask(MyTask task)
        {
            if (task == null)
                throw new NullReferenceException("Task is empty");

            List<ValidationResult> results = new List<ValidationResult>();
            ValidationContext context = new ValidationContext(task);
            string errors = null;
            
            if (!Validator.TryValidateObject(task, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                throw new ArgumentException(errors);
            }
            else
            {
                await Database.Tasks.Update(task);
                await Database.SaveAsync();
            }
        }
    }
}
