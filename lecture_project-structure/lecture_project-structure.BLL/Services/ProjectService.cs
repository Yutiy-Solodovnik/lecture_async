﻿using lecture_EF.BLL.Interfaces;
using lecture_EF.DAL.Entites;
using lecture_EF.DAL.Interfaces;
using lecture_project_structure.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace lecture_EF.BLL.Services
{
    public class ProjectService : IProjectService
    {
        public IQueryBuilder QueryBuilder { get; set; }
        IUnitOfWork Database { get; set; }

        public ProjectService(IUnitOfWork unitOfWork)
        {
            Database = unitOfWork;
            QueryBuilder = new QueryBuilder(unitOfWork);
        }
        public async Task CreateProject(Project project)
        {
            if (project == null)
                throw new NullReferenceException("Project is empty");

            List<ValidationResult> results = new List<ValidationResult>();
            ValidationContext context = new ValidationContext(project);
            string errors = null;
            
            if (!Validator.TryValidateObject(project, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                throw new ArgumentException(errors);
            }
            else
            {
                await Database.Projects.Create(project);
                await Database.SaveAsync();
            }
        }

        public async Task DeleteProject(string id)
        {
            int _id;
            if (int.TryParse(id, out _id) && await Database.Projects.Read(_id) != null)
            {
                await Database.Projects.Delete(_id);
            }
            else
                throw new ArgumentException("Invalid Id");
        }

        public async Task DisposeAsync()
        {
            await Database.DisposeAsync();
        }

        public async Task<IEnumerable<Project>> GetAllProjects()
        {
            return await Database.Projects.ReadAll();
        }

        public async Task<Project> GetProject(string id)
        {
            int _id;
            if (int.TryParse(id, out _id))
                return await Database.Projects.Read(_id);
            else
                throw new ArgumentException("Invalid Id");
        }

        public async Task<List<ProjectInfo>> GetProjectInfo()
        {
            return await QueryBuilder.GetProjectInfo();
        }

        public async Task UpdateProject(Project project)
        {
            if (project == null)
                throw new NullReferenceException("Project is empty");

            List<ValidationResult> results = new List<ValidationResult>();
            ValidationContext context = new ValidationContext(project);
            string errors = null;
            
            if (!Validator.TryValidateObject(project, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                throw new ArgumentException(errors);
            }
            else
            {
                await Database.Projects.Update(project);
                await Database.SaveAsync();
            }
        }
    }
}
