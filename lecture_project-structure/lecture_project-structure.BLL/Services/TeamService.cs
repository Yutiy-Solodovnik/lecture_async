﻿using lecture_EF.BLL.Interfaces;
using lecture_EF.DAL.Entites;
using lecture_EF.DAL.Interfaces;
using lecture_project_structure.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace lecture_EF.BLL.Services
{
    public class TeamService : ITeamService
    {
        public IQueryBuilder QueryBuilder { get; set; }
        IUnitOfWork Database { get; set; }

        public TeamService(IUnitOfWork unitOfWork)
        {
            Database = unitOfWork;
            QueryBuilder = new QueryBuilder(unitOfWork);
        }

        public async Task CreateTeam(Team team)
        {
            if (team == null)
                throw new NullReferenceException("Team is empty");
            List<ValidationResult> results = new List<ValidationResult>();
            ValidationContext context = new ValidationContext(team);
            string errors = null;
            
            if (!Validator.TryValidateObject(team, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                throw new ArgumentException(errors);
            }
            else
            {
                await Database.Teams.Create(team);
                await Database.SaveAsync();
            }
        }

        public async Task DeleteTeam(string id)
        {
            int _id;
            if (int.TryParse(id, out _id) && await Database.Teams.Read(_id) != null)
            {
                await Database.Teams.Delete(_id);
            }
            else
                throw new ArgumentException("Invalid Id");
        }
        public async Task DisposeAsync()
        {
            await Database.DisposeAsync();
        }

        public async Task<IEnumerable<Team>> GetAllTeams()
        {
            return await Database.Teams.ReadAll();
        }

        public async Task<Team> GetTeam(string id)
        {
            int _id;
            if (int.TryParse(id, out _id))
                return await Database .Teams.Read(_id);
            else
                throw new ArgumentException("Invalid Id");
        }

        public async Task<IEnumerable<IGrouping<int?, User>>> GetUsersFromTeam()
        {
            return await QueryBuilder.GetUsersFromTeam();
        }

        public async Task UpdateTeam(Team team)
        {
            if (team == null)
                throw new NullReferenceException("Team is empty");

            List<ValidationResult> results = new List<ValidationResult>();
            ValidationContext context = new ValidationContext(team);
            string errors = null;
            
            if (!Validator.TryValidateObject(team, context, results, true))
            {
                foreach (var error in results)
                {
                    errors += error.ErrorMessage + ", ";
                }
                throw new ArgumentException(errors);
            }
            else
            {
                await Database.Teams.Update(team);
                await Database.SaveAsync();
            }
        }
    }
}
