﻿using lecture_EF.DAL.Entites;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lecture_EF.BLL.Interfaces
{
    public interface ITeamService
    {
        Task CreateTeam(Team team);
        Task<IEnumerable<Team>> GetAllTeams();
        Task<Team> GetTeam(string id);
        Task UpdateTeam(Team team);
        Task<IEnumerable<IGrouping<int?, User>>> GetUsersFromTeam();
        Task DeleteTeam(string id);
        Task DisposeAsync();
    }
}
