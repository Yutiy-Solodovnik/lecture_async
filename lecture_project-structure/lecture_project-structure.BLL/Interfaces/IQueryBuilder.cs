﻿using lecture_EF.DAL.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lecture_project_structure.BLL.Interfaces
{
    public interface IQueryBuilder
    {
        Task UpdateInformation();

        Task GetHierarchy();

        Task<Dictionary<string, int>> GetTasksInProject(int Id);

        Task<List<MyTask>> GetTasksByUser(int Id);
        Task<List<MyTaskInfo>> GetFinishedTasksByUser(int Id);

        Task<IEnumerable<IGrouping<int?, User>>> GetUsersFromTeam();

        Task<List<User>> GetUsersByName();

        Task<UserInfo> GetUserInfo(int Id);

        Task<List<ProjectInfo>> GetProjectInfo();
        Task<List<MyTask>> GetUnfinishedTasksByUser(int id);
    }
}
