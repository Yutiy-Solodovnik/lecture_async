﻿using lecture_EF.DAL.Entites;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace lecture_EF.BLL.Interfaces
{
    public interface IUserService
    {
        Task CreateUser(User team);
        Task<IEnumerable<User>> GetAllUsers();
        Task<User> GetUser(string id);
        Task UpdateUser(User user);
        Task DeleteUser(string id);
        Task<IEnumerable<MyTaskInfo>> GetFinishedTasksByUser(string id);
        Task<List<MyTask>> GetTasksByUser(string id);
        Task<Dictionary<string, int>> GetTasksInProject(string id);
        Task<List<User>> GetUsersByName();
        Task<UserInfo> GetUserInfo(string id);
        Task AddUserToTeam(User user);
        Task DisposeAsync();
        Task<List<MyTask>> GetUnfinishedTasksByUser(string id);
    }
}
