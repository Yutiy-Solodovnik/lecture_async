﻿using lecture_EF.BLL.DTO;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace WebApi.Projects.IntegrationTests
{
    public class ProjectControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory>
    {
        private readonly CustomWebApplicationFactory _factory;

        public ProjectControllerIntegrationTests(CustomWebApplicationFactory factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task Create_Project_With_Right_Body()
        {
            ProjectDTO projectDTO = new ProjectDTO()
            { 
                Id = 0,
                TeamId = 1,
                AuthorId = 1,
                Name = "Test",
                Description = "Right project",
                Deadline = DateTime.Parse("2021-08-25T18:48:06.062331"),
                CreatedAt = DateTime.Parse("2020-06-15T18:48:06.062331")
            };
            string json = JsonConvert.SerializeObject(projectDTO);
            var response = await _factory.Client.PostAsync("api/Projects", new StringContent(json));
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Theory]
        [InlineData("{}")]
        [InlineData("{\"id\":1,\"authorId\":1,\"teamId\":1,\"name\":\"T\",\"description\":\"TestDescription\",\"deadline\":\"2021-08-03T01:08:10.3228394+00:00\",\"createdAt\":\"2019-07-17T06:42:48.376246+00:00\"}")]
        [InlineData("{\"id\":0,\"authorId\":1,\"teamId\":1,\"name\":\"T\",\"description\":\"TestDescription\",\"deadline\":\"2021-08-03T01:08:10.3228394+00:00\",\"createdAt\":\"2019-07-17T06:42:48.376246+00:00\"}")]
        public async Task Create_Project_With_Wrong_Body(string bodyJson)
        {
            ProjectDTO projectDTO  = JsonConvert.DeserializeObject<ProjectDTO>(bodyJson);
            string json = JsonConvert.SerializeObject(projectDTO);
            var response = await _factory.Client.PostAsync("api/Projects", new StringContent(json));
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
