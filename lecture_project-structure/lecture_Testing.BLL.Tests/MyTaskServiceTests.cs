﻿using FakeItEasy;
using lecture_EF.BLL;
using lecture_EF.BLL.Services;
using lecture_EF.DAL.Entites;
using lecture_EF.DAL.Interfaces;
using System;
using System.Collections.Generic;
using Xunit;

namespace lecture_Testing.BLL.Tests
{
    public class MyTaskServiceTests : IDisposable
    {
        private MyTask _task;
        private MyTaskService _taskService;
        private IUnitOfWork _unitOfWork;

        public MyTaskServiceTests()
        {
            _unitOfWork = A.Fake<IUnitOfWork>();
            _taskService = new MyTaskService(_unitOfWork);
            _task = new MyTask()
            {
                Id = 1,
                Name = "Test",
                PerformerId = 3,
                ProjectId = 1,
                Description = "Something",
                State = 2,
                FinishedAt = null,
                CreatedAt = DateTime.Parse("2018-08-25T18:48:06.062331")
            };
        }

        #region ChangingTaskState
        [Fact]
        public void Finish_Already_Finished_Task_ThenThrowArgumentException()
        {
            Assert.Throws<ArgumentException>(() => _taskService.ChangeTaskState(new MyTask()
            {
                Id = 1,
                Name = "Wrong",
                FinishedAt = DateTime.Parse("2018-08-25T18:48:06.062331")
            }));
        }

        [Fact]
        public void Finish_UnFinished_Task()
        {
            _taskService.ChangeTaskState(_task);
            A.CallTo(() => _unitOfWork.Tasks.Update(_task)).MustHaveHappenedOnceExactly();
        }
        #endregion

        public void Dispose()
        {
            _taskService.Dispose();
        }
    }
}