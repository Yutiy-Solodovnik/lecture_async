﻿using FakeItEasy;
using lecture_EF.BLL;
using lecture_EF.BLL.Services;
using lecture_EF.DAL.Entites;
using lecture_EF.DAL.Interfaces;
using lecture_project_structure.BLL.Interfaces;
using System;
using System.Collections.Generic;
using Xunit;

namespace lecture_Testing.BLL.Tests
{
    public class TeamServiceTests
    {
        private Team _team;
        private TeamService _teamService;
        private IQueryBuilder _queryBuilder;
        private IUnitOfWork _unitOfWork;

        public TeamServiceTests()
        {
            _unitOfWork = A.Fake<IUnitOfWork>();
            _teamService = new TeamService(_unitOfWork);
            _queryBuilder = A.Fake<IQueryBuilder>();
            _teamService.QueryBuilder = _queryBuilder;
        }

        [Fact]
        public void Get_Users_From_Team()
        {
            _teamService.GetUsersFromTeam();
            A.CallTo(() => _teamService.QueryBuilder.GetUsersFromTeam()).MustHaveHappenedOnceExactly();
        }

        public void Dispose()
        {
            _teamService.Dispose();
        }
    }
}
