﻿using AutoMapper;
using lecture_EF.BLL.DTO;
using lecture_EF.BLL.Interfaces;
using lecture_EF.DAL.Entites;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading.Tasks;

namespace WebApi.Users.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class UsersController : Controller
    {
        private IUserService _userService;
        private readonly IMapper _mapper;

        public UsersController(IMapper mapper, IUserService userService)
        {
            _mapper = mapper;
            _userService = userService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser()
        {
            try
            {
                Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
                UserDTO userDTO = JsonConvert.DeserializeObject<UserDTO>(todoJson.Result);
                User user = _mapper.Map<User>(userDTO);
                await _userService.CreateUser(user);
                return Ok(user);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser(string id)
        {
            try
            {
                User user = await _userService.GetUser(id);
                UserDTO userDTO = _mapper.Map<UserDTO>(user);
                return Ok(userDTO);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetALLUsers()
        {
            try
            {
                IEnumerable<User> users = await _userService.GetAllUsers();
                IEnumerable<UserDTO> usersDTO = _mapper.Map<IEnumerable<UserDTO>>(users);
                return Ok(usersDTO);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("unfinishedTasksByUser/{id}")]
        public async Task<IActionResult> GetUnfinishedTasksByUser(string id)
        {
            try
            {
                List<MyTask> tasks = await _userService.GetUnfinishedTasksByUser(id);
                List<MyTaskDTO> tasksDTO = _mapper.Map<List<MyTaskDTO>>(tasks);
                return Ok(tasksDTO);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("tasksInProjectOfUser/{id}")]
        public async Task<IActionResult> GetTasksInProject(string id)
        {
            try
            {
                Dictionary<string, int> tasks = await _userService.GetTasksInProject(id);
                return Ok(tasks);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("finishedTasksByUser/{id}")]
        public async Task<IActionResult> GetFinishedTasksByUser(string id)
        {
            try
            {
                IEnumerable<MyTaskInfo> myTasksInfo = await _userService.GetFinishedTasksByUser(id);
                IEnumerable<MyTaskInfoDTO> myTasksInfoDTO = _mapper.Map<IEnumerable<MyTaskInfoDTO>>(myTasksInfo);
                return Ok(myTasksInfoDTO);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("tasksByUser/{id}")]
        public async Task<IActionResult> GetTasksByUser(string id)
        {
            try
            {
                List<MyTask> tasks = await _userService.GetTasksByUser(id);
                List<MyTaskDTO> tasksDTO = _mapper.Map<List<MyTaskDTO>>(tasks);
                return Ok(tasksDTO);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("usersByName")]
        public async Task<IActionResult> GetUsersByName()
        {
            try
            {
                List<User> users = await _userService.GetUsersByName();
                List<UserDTO> usersDTO = _mapper.Map<List<UserDTO>>(users);
                return Ok(usersDTO);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("userInfo/{id}")]
        public async Task<IActionResult> GetUserInfo(string id)
        {
            try
            {
                UserInfo users = await _userService.GetUserInfo(id);
                UserInfoDto usersDTO = _mapper.Map<UserInfoDto>(users);
                return Ok(usersDTO);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(string id)
        {
            try
            {
                await _userService.DeleteUser(id);
                return Ok("Deleted");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public async Task<IActionResult> UpdateUser()
        {
            try
            {
                Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
                UserDTO userDTO = JsonConvert.DeserializeObject<UserDTO>(todoJson.Result);
                User user = _mapper.Map<User>(userDTO);
                await _userService.UpdateUser(user);
                return Ok("Updated");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("addUserToTeam")]
        public async Task<IActionResult> AddUserToTeam()
        {
            try
            {
                Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
                UserDTO userDTO = JsonConvert.DeserializeObject<UserDTO>(todoJson.Result);
                User user = _mapper.Map<User>(userDTO);
                await _userService.AddUserToTeam(user);
                return Ok("Added");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
