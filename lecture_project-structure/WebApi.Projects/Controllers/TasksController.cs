﻿using AutoMapper;
using lecture_EF.BLL.DTO;
using lecture_EF.BLL.Interfaces;
using lecture_EF.DAL.Entites;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading.Tasks;

namespace WebApi.MyTasks.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class TasksController : Controller
    {
        private IMyTaskService _myTaskService;
        private readonly IMapper _mapper;

        public TasksController(IMapper mapper, IMyTaskService myTaskService)
        {
            _mapper = mapper;
            _myTaskService = myTaskService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetMyTask(string id)
        {
            MyTask myTask = await _myTaskService.GetTask(id);
            MyTaskDTO myTaskDTO = _mapper.Map<MyTaskDTO>(myTask);
            return Ok(myTaskDTO);
        }
        
        [HttpGet]
        public async Task<IActionResult> GetALLMyTasks()
        {
            IEnumerable<MyTask> myTasks = await _myTaskService.GetAllTasks();
            IEnumerable<MyTaskDTO> myTasksDTO = _mapper.Map<IEnumerable<MyTaskDTO>>(myTasks);
            return Ok(myTasksDTO);
        }

        [HttpPost]
        public async Task<IActionResult> CreateMyTask()
        {
            try
            {
                Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
                MyTaskDTO myTaskDTO = JsonConvert.DeserializeObject<MyTaskDTO>(todoJson.Result);
                MyTask myTask = _mapper.Map<MyTask>(myTaskDTO);
                await _myTaskService.CreateTask(myTask);
                return Ok(myTask);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMyTask(string id)
        {
            try
            {
                await _myTaskService.DeleteTask(id);
                return Ok("Deleted");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        [HttpPut]
        public async Task<IActionResult> UpdateMyTask()
        {
            try
            {
                Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
                MyTaskDTO myTaskDTO = JsonConvert.DeserializeObject<MyTaskDTO>(todoJson.Result);
                MyTask myTask = _mapper.Map<MyTask>(myTaskDTO);
                await _myTaskService.UpdateTask(myTask);
                return Ok("Updated");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("finishTask")]
        public async Task<IActionResult> ChangeTaskState()
        {
            try
            {
                Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
                MyTaskDTO myTaskDTO = JsonConvert.DeserializeObject<MyTaskDTO>(todoJson.Result);
                MyTask myTask = _mapper.Map<MyTask>(myTaskDTO);
                await _myTaskService.ChangeTaskState(myTask);
                return Ok("Task has been finished");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
