﻿using AutoMapper;
using lecture_EF.BLL.DTO;
using lecture_EF.BLL.Interfaces;
using lecture_EF.DAL.Entites;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Teams.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class TeamsController : Controller
    {
        private ITeamService _teamService;
        private readonly IMapper _mapper;

        public TeamsController(IMapper mapper, ITeamService teamService)
        {
            _mapper = mapper;
            _teamService = teamService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetTeam(string id)
        {
            Team team = await _teamService.GetTeam(id);
            TeamDTO teamDTO = _mapper.Map<TeamDTO>(team);
            return Ok(teamDTO);
        }

        [HttpGet]
        public async Task<IActionResult> GetALLTeams()
        {
            IEnumerable<Team> teams = await _teamService.GetAllTeams();
            IEnumerable<TeamDTO> teamsDTO = _mapper.Map<IEnumerable<TeamDTO>>(teams);
            return Ok(teamsDTO);
        }

        [HttpGet("usersFromTeams")]
        public async Task<IActionResult> GetUsersFromTeam()
        {
            IEnumerable<IGrouping<int?, User>> users = await _teamService.GetUsersFromTeam();
            return Ok(users);
        }

        [HttpPost]
        public async Task<IActionResult> CreateTeam()
        {
            try
            {
                Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
                TeamDTO teamDTO = JsonConvert.DeserializeObject<TeamDTO>(todoJson.Result);
                Team team = _mapper.Map<Team>(teamDTO);
                await _teamService.CreateTeam(team);
                return Ok(team);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTeam(string id)
        {
            try
            {
                await _teamService.DeleteTeam(id);
                return Ok("Deleted");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public async Task<IActionResult> UpdateTeam()
        {
            try
            {
                Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
                TeamDTO teamDTO = JsonConvert.DeserializeObject<TeamDTO>(todoJson.Result);
                Team team = _mapper.Map<Team>(teamDTO);
                await _teamService.UpdateTeam(team);
                return Ok("Updated");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
