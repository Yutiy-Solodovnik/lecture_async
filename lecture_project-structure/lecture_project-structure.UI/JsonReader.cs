﻿using lecture_EF.BLL.DTO;
using lecture_EF.DAL.Entites;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace lecture_EF.UI
{
    public static class JsonReader
    {
        private static string _baseUri = "https://localhost:44300/";
        public static async Task<List<T>> GetEntitiesList<T>() where T : class
        {
            using (HttpClient client = new HttpClient())
            {
                string path = typeof(T).Name switch
                {
                    "Project" => "api/Projects",
                    "User" => "api/Users",
                    "Team" => "api/Teams",
                    "MyTask" => "api/Tasks",
                    _ => throw new ArgumentException("Wrong type")
                };
                client.BaseAddress = new Uri(_baseUri);
                HttpResponseMessage response = await client.GetAsync(path);
                string message = await response.Content.ReadAsStringAsync();
                List<T> entetiesList = await Task.Run(() => JsonConvert.DeserializeObject<List<T>>(message));
                return entetiesList;
            }
        }

        public static async Task AddUser(UserDTO user)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseUri);
                HttpResponseMessage response = await client.PostAsync($"api/Users", new StringContent(JsonConvert.SerializeObject(user)));
            }
        }

        public static async Task<Dictionary<string, int>> GetTasksInProject(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseUri);
                HttpResponseMessage response = await client.GetAsync($"api/Users/tasksInProjectOfUser/{id}");
                string message = await response.Content.ReadAsStringAsync();
                Dictionary<string, int> entetiesList = JsonConvert.DeserializeObject<Dictionary<string, int>>(message);
                return entetiesList;
            }
        }

        public static async Task<List<MyTaskDTO>> GetTasksByUser(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseUri);
                HttpResponseMessage response = await client.GetAsync($"api/Users/tasksByUser/{id}");
                string message = await response.Content.ReadAsStringAsync();
                List<MyTaskDTO> entetiesList = JsonConvert.DeserializeObject<List<MyTaskDTO>>(message);
                return entetiesList;
            }
        }
        public static async Task<List<MyTaskInfo>> GetFinishedTasksByUser(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseUri);
                HttpResponseMessage response = await client.GetAsync($"api/Users/finishedTasksByUser/{id}");
                string message = await response.Content.ReadAsStringAsync();
                List<MyTaskInfo> entetiesList = JsonConvert.DeserializeObject<List<MyTaskInfo>>(message);
                return entetiesList;
            }
        }

        public static async Task<IEnumerable<IGrouping<int?, User>>> GetUsersFromTeam()
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseUri);
                HttpResponseMessage response = await client.GetAsync("api/Teams/usersFromTeams");
                string message = await response.Content.ReadAsStringAsync();
                IEnumerable<IGrouping<int?, User>> entetiesList = JsonConvert.DeserializeObject<IEnumerable<IGrouping<int?, User>>>(message);
                return entetiesList;
            }
        }

        public static async Task<List<User>> GetUsersByName()
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseUri);
                HttpResponseMessage response = await client.GetAsync("api/Users/usersByName");
                string message = await response.Content.ReadAsStringAsync();
                List<User> entetiesList = JsonConvert.DeserializeObject<List<User>>(message);
                return entetiesList;
            }
        }

        public static async Task<UserInfoDto> GetUserInfo(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseUri);
                HttpResponseMessage response = await client.GetAsync($"api/Users/userInfo/{id}");
                string message = await response.Content.ReadAsStringAsync();
                UserInfoDto entetiesList = JsonConvert.DeserializeObject<UserInfoDto>(message);
                return entetiesList;
            }
        }

        public static async Task<List<ProjectInfoDTO>> GetProjectInfo()
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseUri);
                HttpResponseMessage response = await client.GetAsync("api/Projects/projectInfo");
                string message = await response.Content.ReadAsStringAsync();
                List<ProjectInfoDTO> entetiesList = JsonConvert.DeserializeObject<List<ProjectInfoDTO>>(message);
                return entetiesList;
            }
        }

        public static async Task ChangeTaskState(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_baseUri);
                HttpResponseMessage response = await client.GetAsync($"api/Tasks/{id}");
                string message = await response.Content.ReadAsStringAsync();
                MyTask taskToUpdate = await Task.Run(() => JsonConvert.DeserializeObject<MyTask>(message));
                HttpResponseMessage updateResponse = await client.PutAsync($"api/Tasks/finishTask", new StringContent(JsonConvert.SerializeObject(taskToUpdate)));
            }
        }
    }
}
