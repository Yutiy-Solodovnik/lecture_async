﻿using lecture_EF.BLL.DTO;
using lecture_EF.DAL.Entites;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using System.Timers;

namespace lecture_EF.UI
{
    class Program
    {
        static bool isReadyToUpdate = false;
        static async Task Main(string[] args)
        {
            string stringVariant;
            await Update();
            do
            {
                Console.WriteLine();
                Console.WriteLine("\nВыберите действие:\n1. Получить кол-во тасков у проекта конкретного пользователя (по id)" +
                " (словарь, где ключом будет проект, а значением кол-во тасков).\n2. Получить список тасков, назначенных на " +
                "конкретного пользователя (по id), где name таска < 45 символов (коллекция из тасков). \n3. Получить список (id, name) " +
                "из коллекции тасков, которые выполнены (finished) в текущем (2021) году для конкретного пользователя (по id). " +
                "\n4. Получить список (id, имя команды и список пользователей) из коллекции команд, участники которых старше 10 лет," +
                " отсортированных по дате регистрации пользователя по убыванию, а также сгруппированных по командам. " +
                "\n5. Получить список пользователей по алфавиту first_name (по возрастанию) с отсортированными tasks по длине name (по убыванию)." +
                "\n6. Получить следующую структуру (передать Id пользователя в параметры):\n-User\n-Последний проект пользователя(по дате создания)" +
                "\n-Общее кол - во тасков под последним проектом" +
                "\n-Общее кол - во незавершенных или отмененных тасков для пользователя \n-Самый долгий таск пользователя по дате" +
                "\n7. Получить следующую структуру: \n-Проект \n-Самый длинный таск проекта(по описанию) \n-Самый короткий таск проекта(по имени) " +
                "\n-Общее кол - во пользователей в команде проекта, где или описание проекта > 20 символов или кол - во тасков < 3 \n 8. Обновить структуру" +
                "\n 9. Финишировать случаный таск\n 0. Выход");
                Console.WriteLine();
                Validate(out stringVariant, 0, 10);
                switch (byte.Parse(stringVariant))
                {
                    case 1:
                        try
                        {
                            Console.WriteLine("0 - Взять данные с сервера, 1 - Использовать локальный Query BUilder");
                            string stringWayVariant;
                            Validate(out stringWayVariant, 0, 1);
                            switch (byte.Parse(stringWayVariant))
                            {
                                case 0:
                                    Console.WriteLine("Введите id");
                                    foreach (var i in await JsonReader.GetTasksInProject(int.Parse(Console.ReadLine())))
                                    {
                                        Console.WriteLine(i.Key.ToString() + " Количество тасков " + i.Value);
                                    }
                                    break;
                                case 1:
                                    Console.WriteLine("Введите id");
                                    foreach (var i in await QueryBuilder.GetTasksInProject(int.Parse(Console.ReadLine())))
                                    {
                                        Console.WriteLine(i.Key.ToString() + " Количество тасков " + i.Value);
                                    }
                                    break;
                            }

                        }
                        catch (Exception ex)
                        {
                            WriteWrongMessage(ex.Message);
                        }
                        break;
                    case 2:
                        try
                        {
                            Console.WriteLine("0 - Взять данные с сервера, 1 - Использовать локальный Query BUilder");
                            string stringWayVariant;
                            Validate(out stringWayVariant, 0, 1);
                            switch (byte.Parse(stringWayVariant))
                            {
                                case 0:
                                    Console.WriteLine("Введите id");

                                    foreach (var i in await JsonReader.GetTasksByUser(int.Parse(Console.ReadLine())))
                                    {
                                        Console.WriteLine(i.Id + " " + i.Name);
                                    }
                                    break;
                                case 1:
                                    Console.WriteLine("Введите id");

                                    foreach (var i in await QueryBuilder.GetTasksByUser(int.Parse(Console.ReadLine())))
                                    {
                                        Console.WriteLine(i.ToString());
                                    }
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            WriteWrongMessage(ex.Message);
                        }
                        break;
                    case 3:
                        try
                        {
                            Console.WriteLine("0 - Взять данные с сервера, 1 - Использовать локальный Query BUilder");
                            string stringWayVariant;
                            Validate(out stringWayVariant, 0, 1);
                            switch (byte.Parse(stringWayVariant))
                            {
                                case 0:
                                    Console.WriteLine("Введите id");

                                    foreach (var i in await JsonReader.GetTasksByUser(int.Parse(Console.ReadLine())))
                                    {
                                        Console.WriteLine(i.Id + " " + i.Name);
                                    }
                                    break;
                                case 1:
                                    Console.WriteLine("Введите id");

                                    foreach (var i in await QueryBuilder.GetFinishedTasksByUser(int.Parse(Console.ReadLine())))
                                    {
                                        Console.WriteLine($"ID:{i.Id}, имя: {i.Name}");
                                    }
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            WriteWrongMessage(ex.Message);
                        }
                        break;
                    case 4:
                        try
                        {
                            Console.WriteLine("0 - Взять данные с сервера, 1 - Использовать локальный Query BUilder");
                            string stringWayVariant;
                            Validate(out stringWayVariant, 0, 1);
                            switch (byte.Parse(stringWayVariant))
                            {
                                case 0:
                                    Console.WriteLine("Введите id");

                                    foreach (var i in await JsonReader.GetFinishedTasksByUser(int.Parse(Console.ReadLine())))
                                    {
                                        Console.WriteLine($"ID:{i.Id}, имя: {i.Name}");
                                    }
                                    break;
                                case 1:
                                    foreach (var i in await QueryBuilder.GetUsersFromTeam())
                                    {
                                        Console.WriteLine("Id команды: " + i.Key);
                                        foreach (var user in i)
                                            Console.WriteLine(user.ToString());
                                    }
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            WriteWrongMessage(ex.Message);
                        }
                        break;
                    case 5:
                        try
                        {
                            Console.WriteLine("0 - Взять данные с сервера, 1 - Использовать локальный Query BUilder");
                            string stringWayVariant;
                            Validate(out stringWayVariant, 0, 1);
                            switch (byte.Parse(stringWayVariant))
                            {
                                case 0:
                                    foreach (var i in await JsonReader.GetUsersByName())
                                    {
                                        Console.WriteLine(i.ToString());
                                    }
                                    break;
                                case 1:
                                    foreach (var i in await JsonReader.GetUsersByName())
                                    {
                                        Console.WriteLine(i.ToString());
                                    }
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            WriteWrongMessage(ex.Message);
                        }
                        break;
                    case 6:
                        try
                        {
                            Console.WriteLine("0 - Взять данные с сервера, 1 - Использовать локальный Query BUilder");
                            string stringWayVariant;
                            Validate(out stringWayVariant, 0, 1);
                            switch (byte.Parse(stringWayVariant))
                            {
                                case 0:
                                    Console.WriteLine("Введите id");
                                    UserInfoDto userInfo0 = await JsonReader.GetUserInfo(int.Parse(Console.ReadLine()));
                                    Console.WriteLine($"{userInfo0.UserId} {userInfo0.LastProjectId} {userInfo0.AllTasks} {userInfo0.AllUnfinishedTasks} {userInfo0.LongestTaskId}");
                                    break;
                                case 1:
                                    Console.WriteLine("Введите id");
                                    UserInfo userInfo = await QueryBuilder.GetUserInfo(int.Parse(Console.ReadLine()));
                                    Console.WriteLine($"{userInfo.User} {userInfo.LastProject} {userInfo.AllTasks} {userInfo.AllUnfinishedTasks} {userInfo.LongestTask}");
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            WriteWrongMessage(ex.Message);
                        }
                        break;
                    case 7:
                        try
                        {
                            Console.WriteLine("0 - Взять данные с сервера, 1 - Использовать локальный Query BUilder");
                            string stringWayVariant;
                            Validate(out stringWayVariant, 0, 1);
                            switch (byte.Parse(stringWayVariant))
                            {
                                case 0:
                                    foreach (var i in await JsonReader.GetProjectInfo())
                                    {
                                        Console.WriteLine($"{i.ProjectId} | Самый длинный таск {i.LongestTaskId} |" +
                                            $"Самый короткий таск {i.ShortestTaskId} | Количество пользователей {i.AllUsersOnProject}");
                                    }
                                    break;
                                case 1:
                                    foreach (var i in await QueryBuilder.GetProjectInfo())
                                    {
                                        Console.WriteLine($"{i.Project.ToString()} | Самый длинный таск {i.LongestTask?.ToString()} |" +
                                            $"Самый короткий таск {i.ShortestTask?.ToString()} | Количество пользователей {i.AllUsersOnProject}");
                                    }
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            WriteWrongMessage(ex.Message);
                        }
                        break;
                    case 8:
                        try
                        {
                            await Update();
                        }
                        catch (Exception ex)
                        {
                            WriteWrongMessage(ex.Message);
                        }
                        break;
                    case 9:
                        try
                        {
                            Console.WriteLine("0 - Запустить финиширование тасков, 1 - Прекратить финиширование тасков");
                            string stringWayVariant;
                            Validate(out stringWayVariant, 0, 1);
                            switch (byte.Parse(stringWayVariant))
                            {
                                case 0:
                                    isReadyToUpdate = true;
                                    MarkRandomTaskWithDelayInPeriod();
                                    break;
                                case 1:
                                    isReadyToUpdate = false;
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            WriteWrongMessage(ex.Message);
                        }
                        break;
                }
            } while (byte.Parse(stringVariant) != 0);

            static void Validate(out string stringVariant, int from, int to)
            {
                do
                {
                    Console.WriteLine();
                    Console.ForegroundColor = ConsoleColor.Green;
                    stringVariant = Console.ReadLine();
                    Console.ForegroundColor = ConsoleColor.White;
                    if (!IsRightInDiapason(stringVariant, from, to))
                    {
                        WriteWrongMessage("Ошибка! Неверный ввод");
                    }
                }
                while (!IsRightInDiapason(stringVariant, from, to));
            }

            static bool IsRightInDiapason(string stringVariant, int from, int to)
            {
                int variant;

                if (int.TryParse(stringVariant, out variant))
                {
                    if (variant >= from && variant <= to)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }

            static void WriteWrongMessage(string message)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(message);
                Console.ForegroundColor = ConsoleColor.White;
            }

            static async Task Update()
            {
                Console.WriteLine("Выполняется считывание данных...");
                await QueryBuilder.UpdateInformation();
            }


        }
        static async Task MarkRandomTaskWithDelayInPeriod()
        {
            while (isReadyToUpdate)
            {
                var markedTaskId = await QueryBuilder.MarkRandomTaskWithDelay(1000);
                Console.WriteLine("Таск с ID - " + markedTaskId + " успешно закончен");
            }
        }
    }
}
