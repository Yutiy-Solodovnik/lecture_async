﻿using lecture_EF.DAL.Entites;
using lecture_project_structure.DAL.EF;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace lecture_EF.DAL.EF
{
    public class ProjectsContext : DbContext
    {
        public ProjectsContext(DbContextOptions<ProjectsContext> options)
            : base(options)
        { }

        public DbSet<MyTask> MyTasks {get; set;}
        public DbSet<Project> Projects {get; set;}
        public DbSet<Team> Teams {get; set;}
        public DbSet<User> Users {get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Seed();
        }

        public async Task SaveChangesAsync()
        {
            await base.SaveChangesAsync();
        }
    }
}
