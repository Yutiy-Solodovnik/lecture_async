﻿using lecture_EF.DAL.EF;
using lecture_EF.DAL.Entites;
using lecture_EF.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lecture_EF.DAL.Repositories
{
    public class ProjectRepository : IRepository<Project>
    {
        private ProjectsContext db;
        public ProjectRepository(ProjectsContext context)
        {
            db = context;
        }
        public async Task Create(Project item)
        {
            await db.Projects.AddAsync(item);
            await db.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {           
            db.Projects.Remove(await db.Projects.SingleOrDefaultAsync(x => x.Id == id));
            await db.SaveChangesAsync();
        }

        public async Task<Project> Read(int id)
        {
            return await db.Projects.FindAsync(id);
        }

        public async Task<IEnumerable<Project>> ReadAll()
        {
            return await db.Projects.ToListAsync();
        }

        public async Task Update(Project item)
        {
            Project project = await Read(item.Id);
            if (project != null)
            {
                project.Name = item.Name;
                project.Team = item.Team;
                project.TeamId = item.TeamId;
                project.Tasks = item.Tasks;
                project.Author = item.Author;
                project.AuthorId = item.AuthorId;
                project.Description = item.Description;
                project.Deadline = item.Deadline;
                project.CreatedAt = item.CreatedAt;
                db.Entry(project).State = EntityState.Modified;
                await db.SaveChangesAsync();
            }
        }
    }
}
