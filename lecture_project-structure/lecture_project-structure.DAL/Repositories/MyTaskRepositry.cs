﻿using lecture_EF.DAL.EF;
using lecture_EF.DAL.Entites;
using lecture_EF.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lecture_EF.DAL.Repositories
{
    public class MyTaskRepositry : IRepository<MyTask>
    {
        private ProjectsContext db;
        public MyTaskRepositry(ProjectsContext context)
        {
            db = context;
        }
        public async Task Create(MyTask item)
        {
            await db.MyTasks.AddAsync(item);
            await db.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            db.MyTasks.Remove(await db.MyTasks.SingleOrDefaultAsync(x => x.Id == id));
            await db.SaveChangesAsync();
        }

        public async Task<MyTask> Read(int id)
        {
            return await db.MyTasks.FindAsync(id);
        }

        public async Task<IEnumerable<MyTask>> ReadAll()
        {
            return await db.MyTasks.ToListAsync();
        }

        public async Task Update(MyTask item)
        {
            MyTask task = await Read(item.Id);
            if (task != null)
            {
                task.Name = item.Name;
                task.Performer = item.Performer;
                task.PerformerId = item.PerformerId;
                task.ProjectId = item.ProjectId;
                task.State = item.State;
                task.Description = item.Description;
                task.CreatedAt = item.CreatedAt;
                task.FinishedAt = item.FinishedAt;
                db.Entry(task).State = EntityState.Modified;
                await db.SaveChangesAsync();
            }
        }
    }
}
