﻿using lecture_EF.DAL.EF;
using lecture_EF.DAL.Entites;
using lecture_EF.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lecture_EF.DAL.Repositories
{
    public class TeamRepository : IRepository<Team>
    {
        private ProjectsContext db;
        public TeamRepository(ProjectsContext context)
        {
            db = context;
        }
        public async Task Create(Team item)
        {
            await db.Teams.AddAsync(item);
            await db.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            db.Teams.Remove(await db.Teams.SingleOrDefaultAsync(x => x.Id == id));
            await db.SaveChangesAsync();
        }

        public async Task<Team> Read(int id)
        {
            return await db.Teams.FindAsync(id);
        }

        public async Task<IEnumerable<Team>> ReadAll()
        {
            return await db.Teams.ToListAsync();
        }

        public async Task Update(Team item)
        {
            Team team = await Read(item.Id);
            if (team != null)
            {
                team.Name = item.Name;
                team.CreatedAt = item.CreatedAt;
                db.Entry(team).State = EntityState.Modified;
                await db.SaveChangesAsync();
            }
        }
    }
}
