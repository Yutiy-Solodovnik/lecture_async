﻿using System;
using System.ComponentModel.DataAnnotations;

namespace lecture_EF.DAL.Entites
{
    public class MyTask
    {
        [Key]
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public User Performer { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        [StringLength(50, MinimumLength = 2,
        ErrorMessage = "Name should be minimum 2 characters and a maximum of 50 characters")]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [DataType(DataType.Text)]
        public string Description { get; set; }
        public int State { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime CreatedAt { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? FinishedAt { get; set; }
        public override string ToString()
        {
            return $"Название: {Name}|Разработчик: {Performer.FirstName + " " + Performer.LastName}" +
                $"|Описание {Description}|Состояние {State}|Создан: {CreatedAt}|Завершен: {FinishedAt}";
        }
    }
}
