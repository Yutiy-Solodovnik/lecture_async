﻿using System;
using System.ComponentModel.DataAnnotations;

namespace lecture_EF.DAL.Entites
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        public int? TeamId { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        [StringLength(50, MinimumLength = 2,
        ErrorMessage = "First name should be minimum 2 characters and a maximum of 50 characters")]
        [DataType(DataType.Text)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        [StringLength(50, MinimumLength = 2,
        ErrorMessage = "Last name should be minimum 2 characters and a maximum of 50 characters")]
        [DataType(DataType.Text)]
        public string LastName { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime RegisteredAt { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime BirthDay { get; set; }
        public override string ToString()
        {
            return $"{FirstName}|{LastName}|Почта: {Email}|Зарегестрирован: {RegisteredAt}|День рождения: {BirthDay}";
        }
    }
}
