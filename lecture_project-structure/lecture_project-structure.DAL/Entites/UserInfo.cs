﻿using System;

namespace lecture_EF.DAL.Entites
{
    public class UserInfo
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
         
        public int AllTasks { get; set; }
        public int AllUnfinishedTasks { get; set; }
         
        public MyTask LongestTask { get; set; }
    }
}
