﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using lecture_EF.DAL.EF;

namespace lecture_project_structure.DAL.Migrations
{
    [DbContext(typeof(ProjectsContext))]
    partial class ProjectsContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.7")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("lecture_EF.DAL.Entites.MyTask", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("FinishedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<int>("PerformerId")
                        .HasColumnType("int");

                    b.Property<int>("ProjectId")
                        .HasColumnType("int");

                    b.Property<int>("State")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("PerformerId");

                    b.HasIndex("ProjectId");

                    b.ToTable("MyTasks");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CreatedAt = new DateTime(2017, 10, 31, 11, 39, 16, 990, DateTimeKind.Local).AddTicks(799),
                            Description = "Eveniet nihil asperiores esse minima.",
                            Name = "index",
                            PerformerId = 5,
                            ProjectId = 1,
                            State = 2
                        },
                        new
                        {
                            Id = 2,
                            CreatedAt = new DateTime(2020, 5, 16, 1, 50, 46, 86, DateTimeKind.Local).AddTicks(832),
                            Description = "Quo sint aut et ea voluptatem omnis ut.",
                            Name = "real-time",
                            PerformerId = 4,
                            ProjectId = 2,
                            State = 2
                        },
                        new
                        {
                            Id = 3,
                            CreatedAt = new DateTime(2019, 2, 15, 17, 6, 52, 60, DateTimeKind.Local).AddTicks(666),
                            Description = "Eum a eum.",
                            Name = "product Direct utilize",
                            PerformerId = 3,
                            ProjectId = 2,
                            State = 2
                        });
                });

            modelBuilder.Entity("lecture_EF.DAL.Entites.Project", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AuthorId")
                        .HasColumnType("int");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("Deadline")
                        .HasColumnType("datetime2");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<int>("TeamId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("AuthorId");

                    b.HasIndex("TeamId");

                    b.ToTable("Projects");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            AuthorId = 5,
                            CreatedAt = new DateTime(2019, 7, 17, 9, 42, 48, 376, DateTimeKind.Local).AddTicks(2460),
                            Deadline = new DateTime(2021, 8, 3, 4, 8, 10, 322, DateTimeKind.Local).AddTicks(8394),
                            Description = "Repellendus expedita dolorum saepe ut culpa nobis sunt itaque labore.",
                            Name = "open architecture Outdoors, Grocery & Baby Dynamic",
                            TeamId = 6
                        },
                        new
                        {
                            Id = 2,
                            AuthorId = 6,
                            CreatedAt = new DateTime(2020, 8, 25, 20, 49, 50, 451, DateTimeKind.Local).AddTicks(8054),
                            Deadline = new DateTime(2021, 9, 12, 22, 17, 47, 233, DateTimeKind.Local).AddTicks(5223),
                            Description = "Et doloribus et temporibus.",
                            Name = "backing up Handcrafted Fresh Shoes challenge",
                            TeamId = 3
                        },
                        new
                        {
                            Id = 3,
                            AuthorId = 7,
                            CreatedAt = new DateTime(2021, 1, 30, 16, 38, 53, 883, DateTimeKind.Local).AddTicks(8745),
                            Deadline = new DateTime(2021, 7, 24, 15, 7, 31, 935, DateTimeKind.Local).AddTicks(7846),
                            Description = "Non voluptatem voluptas libero.",
                            Name = "Village",
                            TeamId = 4
                        },
                        new
                        {
                            Id = 4,
                            AuthorId = 10,
                            CreatedAt = new DateTime(2020, 3, 15, 22, 33, 15, 673, DateTimeKind.Local).AddTicks(1141),
                            Deadline = new DateTime(2021, 7, 21, 11, 32, 51, 335, DateTimeKind.Local).AddTicks(4654),
                            Description = "Quia et tempora hic pariatur voluptatem doloribus sunt.",
                            Name = "Dam",
                            TeamId = 1
                        },
                        new
                        {
                            Id = 5,
                            AuthorId = 9,
                            CreatedAt = new DateTime(2019, 7, 3, 6, 39, 1, 997, DateTimeKind.Local).AddTicks(8679),
                            Deadline = new DateTime(2021, 6, 25, 14, 25, 0, 711, DateTimeKind.Local).AddTicks(1264),
                            Description = "Soluta non sed assumenda.",
                            Name = "iterate project",
                            TeamId = 1
                        },
                        new
                        {
                            Id = 6,
                            AuthorId = 8,
                            CreatedAt = new DateTime(2021, 3, 15, 21, 47, 35, 933, DateTimeKind.Local).AddTicks(5401),
                            Deadline = new DateTime(2021, 9, 14, 5, 53, 20, 800, DateTimeKind.Local).AddTicks(3038),
                            Description = "Voluptatibus error ut id libero quam natus molestias natus.",
                            Name = "Libyan Dinar Netherlands Antilles",
                            TeamId = 2
                        });
                });

            modelBuilder.Entity("lecture_EF.DAL.Entites.Team", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.HasKey("Id");

                    b.ToTable("Teams");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CreatedAt = new DateTime(2019, 8, 25, 18, 48, 6, 62, DateTimeKind.Local).AddTicks(3310),
                            Name = "Denesik - Greenfelder"
                        },
                        new
                        {
                            Id = 2,
                            CreatedAt = new DateTime(2017, 3, 31, 5, 29, 28, 374, DateTimeKind.Local).AddTicks(504),
                            Name = "Durgan Group"
                        },
                        new
                        {
                            Id = 3,
                            CreatedAt = new DateTime(2019, 2, 21, 17, 47, 30, 379, DateTimeKind.Local).AddTicks(7852),
                            Name = "Kassulke LLC"
                        },
                        new
                        {
                            Id = 4,
                            CreatedAt = new DateTime(2018, 8, 28, 11, 18, 46, 416, DateTimeKind.Local).AddTicks(342),
                            Name = "Harris LLC"
                        },
                        new
                        {
                            Id = 5,
                            CreatedAt = new DateTime(2019, 4, 3, 12, 58, 33, 17, DateTimeKind.Local).AddTicks(8179),
                            Name = "Mitchell Inc"
                        },
                        new
                        {
                            Id = 6,
                            CreatedAt = new DateTime(2016, 10, 5, 10, 57, 2, 842, DateTimeKind.Local).AddTicks(7653),
                            Name = "Smitham Group"
                        });
                });

            modelBuilder.Entity("lecture_EF.DAL.Entites.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("BirthDay")
                        .HasColumnType("datetime2");

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<DateTime>("RegisteredAt")
                        .HasColumnType("datetime2");

                    b.Property<int?>("TeamId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("Users");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            BirthDay = new DateTime(1953, 12, 23, 4, 31, 55, 625, DateTimeKind.Local).AddTicks(1180),
                            Email = "Vivian99@yahoo.com",
                            FirstName = "Vivian",
                            LastName = "Mertz",
                            RegisteredAt = new DateTime(2018, 10, 19, 1, 37, 40, 756, DateTimeKind.Local).AddTicks(2662),
                            TeamId = 4
                        },
                        new
                        {
                            Id = 2,
                            BirthDay = new DateTime(1992, 9, 27, 23, 27, 3, 523, DateTimeKind.Local).AddTicks(6015),
                            Email = "Theresa_Gottlieb66@yahoo.com",
                            FirstName = "Theresa",
                            LastName = "Gottlieb",
                            RegisteredAt = new DateTime(2019, 12, 4, 18, 52, 4, 372, DateTimeKind.Local).AddTicks(7619),
                            TeamId = 3
                        },
                        new
                        {
                            Id = 3,
                            BirthDay = new DateTime(2007, 1, 1, 7, 10, 18, 898, DateTimeKind.Local).AddTicks(8690),
                            Email = "Brandy.Witting@gmail.com",
                            FirstName = "Brandy",
                            LastName = "Witting",
                            RegisteredAt = new DateTime(2019, 1, 10, 4, 51, 56, 714, DateTimeKind.Local).AddTicks(8896),
                            TeamId = 3
                        },
                        new
                        {
                            Id = 4,
                            BirthDay = new DateTime(1980, 2, 20, 18, 32, 12, 635, DateTimeKind.Local).AddTicks(8667),
                            Email = "Theresa82@hotmail.com",
                            FirstName = "Theresa",
                            LastName = "Ebert",
                            RegisteredAt = new DateTime(2017, 5, 14, 5, 37, 44, 448, DateTimeKind.Local).AddTicks(6766),
                            TeamId = 3
                        },
                        new
                        {
                            Id = 5,
                            BirthDay = new DateTime(1954, 4, 9, 8, 4, 50, 470, DateTimeKind.Local).AddTicks(9098),
                            Email = "Alfredo_Simonis@yahoo.com",
                            FirstName = "Alfredo",
                            LastName = "Simonis",
                            RegisteredAt = new DateTime(2019, 10, 14, 19, 40, 52, 277, DateTimeKind.Local).AddTicks(2028),
                            TeamId = 2
                        },
                        new
                        {
                            Id = 6,
                            BirthDay = new DateTime(2009, 5, 12, 5, 9, 16, 237, DateTimeKind.Local).AddTicks(3321),
                            Email = "Joanna25@hotmail.com",
                            FirstName = "Joanna",
                            LastName = "Botsford",
                            RegisteredAt = new DateTime(2019, 5, 14, 21, 2, 44, 99, DateTimeKind.Local).AddTicks(5821),
                            TeamId = 2
                        },
                        new
                        {
                            Id = 7,
                            BirthDay = new DateTime(1962, 2, 26, 10, 8, 59, 71, DateTimeKind.Local).AddTicks(1820),
                            Email = "Ann.Langworth@hotmail.com",
                            FirstName = "Ann",
                            LastName = "Langworth",
                            RegisteredAt = new DateTime(2017, 8, 9, 8, 47, 42, 369, DateTimeKind.Local).AddTicks(9036),
                            TeamId = 2
                        },
                        new
                        {
                            Id = 8,
                            BirthDay = new DateTime(1981, 1, 10, 13, 38, 30, 529, DateTimeKind.Local).AddTicks(222),
                            Email = "Christie.Gusikowski@hotmail.com",
                            FirstName = "Christie",
                            LastName = "Gusikowski",
                            RegisteredAt = new DateTime(2019, 10, 7, 10, 41, 43, 246, DateTimeKind.Local).AddTicks(699),
                            TeamId = 6
                        },
                        new
                        {
                            Id = 9,
                            BirthDay = new DateTime(1963, 9, 19, 20, 24, 20, 711, DateTimeKind.Local).AddTicks(674),
                            Email = "Lori_Vandervort@hotmail.com",
                            FirstName = "Lori",
                            LastName = "Vandervort",
                            RegisteredAt = new DateTime(2018, 9, 1, 1, 41, 7, 688, DateTimeKind.Local).AddTicks(8922),
                            TeamId = 1
                        },
                        new
                        {
                            Id = 10,
                            BirthDay = new DateTime(1994, 5, 9, 11, 53, 45, 921, DateTimeKind.Local).AddTicks(8955),
                            Email = "Micheal71@hotmail.com",
                            FirstName = "Micheal",
                            LastName = "Franecki",
                            RegisteredAt = new DateTime(2020, 7, 6, 22, 33, 41, 938, DateTimeKind.Local).AddTicks(340),
                            TeamId = 5
                        },
                        new
                        {
                            Id = 11,
                            BirthDay = new DateTime(1961, 9, 11, 8, 38, 14, 838, DateTimeKind.Local).AddTicks(2487),
                            Email = "Felicia.Kirlin74@yahoo.com",
                            FirstName = "Felicia",
                            LastName = "Kirlin",
                            RegisteredAt = new DateTime(2020, 12, 5, 22, 40, 46, 113, DateTimeKind.Local).AddTicks(3753),
                            TeamId = 5
                        });
                });

            modelBuilder.Entity("lecture_EF.DAL.Entites.MyTask", b =>
                {
                    b.HasOne("lecture_EF.DAL.Entites.User", "Performer")
                        .WithMany()
                        .HasForeignKey("PerformerId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("lecture_EF.DAL.Entites.Project", null)
                        .WithMany("Tasks")
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Performer");
                });

            modelBuilder.Entity("lecture_EF.DAL.Entites.Project", b =>
                {
                    b.HasOne("lecture_EF.DAL.Entites.User", "Author")
                        .WithMany()
                        .HasForeignKey("AuthorId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("lecture_EF.DAL.Entites.Team", "Team")
                        .WithMany()
                        .HasForeignKey("TeamId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Author");

                    b.Navigation("Team");
                });

            modelBuilder.Entity("lecture_EF.DAL.Entites.Project", b =>
                {
                    b.Navigation("Tasks");
                });
#pragma warning restore 612, 618
        }
    }
}
