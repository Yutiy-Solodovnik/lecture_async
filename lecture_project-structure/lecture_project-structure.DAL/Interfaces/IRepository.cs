﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace lecture_EF.DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        Task<IEnumerable<T>> ReadAll();
        Task<T> Read(int id);
        Task Create(T item);
        Task Update(T item);
        Task Delete(int id);
    }
}
